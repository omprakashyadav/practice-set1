import psycopg2

# conn = psycopg2.connect(
#     database="testbb",
#     user="postgress",
#     password="demo1234",
#     port="5432"
# )
 
# cur = conn.cursor()
# cur.execute("INSERT INTO company(ID ,name ,age)values(4,'op',21)")
# conn.commit()
# cur.execute("SELECT * from company")
# rows= cur.fetchall()

# for row in rows():
#     print("ID:",row[0])
#     print("name:", row[1])
#     print("age:", row[2])
#     print("\n")
# print("execute the query")
# conn.close()

# class person:
    
#     def __init__(self, firstname, lastname):
#         self.fname = firstname
#         self.lname = lastname

#     def get_content(self):
#         return self.fname

#     def set_content(self, x):
#         self.fname = x

#     def getName(self):
#         return self.lname
        
#     def setName(self ,y):
#         self.lname = y 


# p = person('omprakash' , 'yadav')
# print(p.get_content)
# p.set_content('op')
# p.setName('ssdf')
# print(p.getName)

class DBConnection(object):
    def __init__(self, database, user,
                 password, port ):

        self.port = port
        self.database = database
        self.user = user
        self.password = password

    def __enter__(self):
        self.dbconn = psycopg2.connect(
                                     "DATABASE={};".format(self.database) +
                                     "UID={};".format(self.user) +
                                     "PWD={};".format(self.password) +
                                     "POT{};".format(self.port)+
                                     "CHARSET=UTF8",
                                     # "",
                                     ansi=True)

        return self.dbconn

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.dbconn.close()

p = DBConnection('db','12','pwd@!23','5432')
print(p.__enter__)
