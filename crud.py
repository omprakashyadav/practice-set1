
from http.server import HTTPServer, BaseHTTPRequestHandler
import cgi

userlist = ['user 1', 'user 2', 'user 3']

booklist = ['book 1', 'book 2', 'book3']
class Detail(BaseHTTPRequestHandler):
    pass
class postdatauser(BaseHTTPRequestHandler):
    

    def do_GET(self):
      if self.path.endswith('/userlist'):
        self.send_response(200)
        self.send_header('content-type', 'text/html')
        self.end_headers()

        output = ''
        output += '<html><body>'
        output += '<h1>User and Book list</h1>'
        for i in userlist:
            output += i

            output += '</br>'
        output += '</body></html>'
        self.wfile.write(output.encode())
        
        if self.path.endswith('/user'):
          self.send_response(200)
          self.send_header('content-type', 'text/html')
          self.end_headers()

          output = ''
          output += '<html><body>'
          output += '<h1>Add new user</h1>'

          output += '<form method="POST" enctype="multipart/form-data" action="/userlist/user">'
          output += '<input name="task" type="text" palceholder="add new user">'
          output += '<input type ="submit" value="Add">'
          output += '</form>'
          output += '</body></html>'

    def do_POST(self):
        if self.path.endswith('/newuser'):
            ctype, pdict = cgi.parse_header(self.headers.get('content-type'))
            pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
            content_len = int(self.headers.get('Content-length'))
            pdict['CONTENT-LENGTH'] = content_len
            if ctype == 'multipart/form-data':
                 fields = cgi.parse_multipart(self.rfile, pdict)
                 new_task = fields.get('task')
                 userlist.append(new_task[0])

            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.send_header('location', '/tasklist')
            self.end_headers()
           
class postdetailofbook(BaseHTTPRequestHandler):

    def do_GET(self):
      if self.path.endswith('/booklist'):
        self.send_response(200)
        self.send_header('content-type', 'text/html')
        self.end_headers()

        output = ''
        output += '<html><body>'
        output += '<h1> Book list</h1>'
        for i in booklist:
            output += i

            output += '</br>'
        output += '</body></html>'
        self.wfile.write(output.encode())

        if self.path.endswith('/book'):
          self.send_response(200)
          self.send_header('content-type', 'text/html')
          self.end_headers()

          output = ''
          output += '<html><body>'
          output += '<h1>Add new book</h1>'

          output += '<form method="POST" enctype="multipart/form-data" action="/booklist/book">'
          output += '<input name="task" type="text" palceholder="add new user">'
          output += '<input type ="submit" value="Add">'
          output += '</form>'
          output += '</body></html>'

    def do_post(self): 
          if self.path.endswith('/book'):
             ctype,pdict =cgi.parse_header(self.headers.get('content-type'))
             pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
             content_len = int(self.headers.get('Content-length'))
             pdict['CONTENT-LENGTH'] = content_len
             if ctype == 'multipart/form-data':
                 fields =cgi.parse_multipart(self.rfile,pdict)
                 new_task=fields.get('task')
                 booklist.append(new_task[0])
             self.send_response(200)
             self.send_header('content-type','text/html')
             self.send_header('location','/tasklist')
             self.end_headers()  


def main():
    port = 50738
    server_address = ('localhost', port)
    server = HTTPServer(server_address, Detail)
    print("serving at port ", port)
    server.serve_forever()

if __name__ == "__main__":
         main()
