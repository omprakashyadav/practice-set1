# import http.server
# import socketserver
# import io


# class Handler(http.server.SimpleHTTPRequestHandler):
#     def do_GET(self):
#         output = io.BytesIO()
#         self.send_response(200)
#         self.send_header('content-type','html')
#         self.end_headers()
#         self.wfile.write(output.read())
#         return 

# print('Server listening on port 8001...')
# http_server = socketserver.TCPServer(('localhost', 8001), Handler)
# http_server.serve_forever()

from http.server import HTTPServer, BaseHTTPRequestHandler


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'<html><body>hello Omprakash</body></html>')

http_server = HTTPServer(('localhost', 8001), SimpleHTTPRequestHandler)
http_server.serve_forever()
