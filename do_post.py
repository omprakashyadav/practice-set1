from http.server import HTTPServer, BaseHTTPRequestHandler
import cgi

tasklist= ['Task 1','Task 2','Task 3']
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
      if self.path.endswith('/tasklist'): 
        self.send_response(200)
        self.send_header('content-type','text/html')
        self.end_headers()
    
        output=''
        output+='<html><body>'
        output+='<h1>User and Book list</h1>'
        for i in tasklist:
            output +=i
       
            output+='</br>'
        output+='</body></html>'
        self.wfile.write(output.encode())

      if self.path.endswith('/new'):
          self.send_response(200)
          self.send_header('content-type','text/html')
          self.end_headers()

          output  = ''
          output +='<html><body>'
          output +='<h1>Add new task</h1>'

          output +='<form method="POST" enctype="multipart/form-data" action="/tasklist/new">'
          output +='<input name="task" type="text" palceholder="add new task">'
          output +='<input type ="submit" value="Add">'
          output +='</form>'
          output +='</body></html>'

          self.wfile.write(output.encode())

      if self.path.endswith('/newuser'):
          self.send_response(200)
          self.send_header('content-type', 'text/html')
          self.end_headers()

          output = ''
          output += '<html><body>'
          output += '<h1>Add new newuser</h1>'

          output += '<form method="POST" enctype="multipart/form-data" action="/tasklist/newuser">'
          output += '<input name="task" type="text" palceholder="add new newuser">'
          output += '<input type ="submit" value="Add">'
          output += '</form>'
          output += '</body></html>'

          self.wfile.write(output.encode())

      if self.path.endswith('/book'):
          self.send_response(200)
          self.send_header('content-type', 'text/html')
          self.end_headers()

          output = ''
          output += '<html><body>'
          output += '<h1>Add new book</h1>'

          output += '<form method="POST" enctype="multipart/form-data" action="/tasklist/book">'
          output += '<input name="task" type="text" palceholder="add new Book">'
          output += '<input type ="submit" value="Add">'
          output += '</form>'
          output += '</body></html>'

          self.wfile.write(output.encode())
    


      if self.path.endswith('/remove'):
          listIdpath=self.path.split('/')[2]
          print(listIdpath)
          self.send_response(200)
          self.send_header('content-type', 'text/html')
          self.end_headers()
          output = ''
          output+='<html><body>'
          output+='<h2>Remove Task: %s</h2>' %listIdpath.replace('%20','')
          output += '<form action="/tasklist/%s/remove" method="POST" enctype="multipart/form-data">' %listIdpath
          output +='<input value="Remove" type="submit"></form>'
          output +='<a href="/tasklist">cancel</a>'
          output+='</html></body>'
          self.wfile.write(output.encode())

    def do_POST(self):
         if self.path.endswith('/new'):
             ctype,pdict =cgi.parse_header(self.headers.get('content-type'))
             pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
             content_len = int(self.headers.get('Content-length'))
             pdict['CONTENT-LENGTH'] = content_len
             if ctype == 'multipart/form-data':
                 fields =cgi.parse_multipart(self.rfile,pdict)
                 new_task=fields.get('task')
                 tasklist.append(new_task[0])
             self.send_response(200)
             self.send_header('content-type','text/html')
             self.send_header('location','/tasklist')
             self.end_headers()

         if self.path.endswith('/newuser'):
             ctype,pdict =cgi.parse_header(self.headers.get('content-type'))
             pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
             content_len = int(self.headers.get('Content-length'))
             pdict['CONTENT-LENGTH'] = content_len
             if ctype == 'multipart/form-data':
                 fields =cgi.parse_multipart(self.rfile,pdict)
                 new_task=fields.get('task')
                 tasklist.append(new_task[0])
             self.send_response(200)
             self.send_header('content-type','text/html')
             self.send_header('location','/tasklist')
             self.end_headers() 

         if self.path.endswith('/book'):
             ctype,pdict =cgi.parse_header(self.headers.get('content-type'))
             pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
             content_len = int(self.headers.get('Content-length'))
             pdict['CONTENT-LENGTH'] = content_len
             if ctype == 'multipart/form-data':
                 fields =cgi.parse_multipart(self.rfile,pdict)
                 new_task=fields.get('task')
                 tasklist.append(new_task[0])
             self.send_response(200)
             self.send_header('content-type','text/html')
             self.send_header('location','/tasklist')
             self.end_headers()       
         
         if self.path.endswith('/remove'):
             listIdpath=self.path.split('/')[2]
             ctype, pdict = cgi.parse_header(self.headers.get('content-type'))
             if ctype == 'multipart/form-data':
                 list_item=listIdpath.replace('%20',' ')
                 tasklist.remove(list_item)
                
             self.send_response(200)
             self.send_header('content-type','text/html')
             self.send_header('location','/tasklist')
             self.end_headers()

          



def main():
    port = 50738
    server_address=('localhost',port)
    server= HTTPServer(server_address, SimpleHTTPRequestHandler)
    print("serving at port ", port)
    server.serve_forever()

if __name__ == "__main__":
         main()
