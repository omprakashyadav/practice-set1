# def my_function(k):
#     if(k>0):
#       result = k+ my_function(k-1) 
#       print(result)
#     else:
#         result = 0
#     return result
# print("\n\nRecursion Example Results")        
# my_function(6)    
 
class person:
     def __init__(self,name,age):
        self.name = name
        self.age=age
        
object = person("op",22)

print(object.age,object.name) 

class Employee:

        #attribute
        attre1 = "Human"
        attre2 = "op"

        #method
        def fun(self):
                print("I am a ", self.attre1)
                print("I am a ", self.attre2)

#object cretaion  
object =Employee()
print(object.attre1)
object.fun()

# constructor

class person1:
        #init constructor
        def __init__(self,name):
                self.name=name

           #sample method
        def sayhi(self):
                 print("Hello , my name is" ,self.name) 

object = person1("omprakash")
object.sayhi()

# intance and variable

class parrot:
       # class variable   
        bird = "parrot"
         #init method or constructor
        def __init__(self,color,breed):

                #instance variable
                self.breed = breed
                self.color = color
          
object1 = parrot("brown", " lovebirds")
object2 = parrot("green", " budgerigars")

print('object1 details:')
print('lovebird is a', object1.bird)
print('Breed: ', object1.breed)
print('Color: ', object1.color)

print(" object2 Deatil ")
print("budgerigars is a", object2.bird)
print("breed :" ,object2.breed)
print("color :" ,object2.color)


#Defining instance variable using the normal method.
class Peacock:
        bird = "Peacock"

        def __init__(self ,breed):
                self.breed = breed


        def setFunction(self,color):
                self.color = color 

        def getFunction(self):
              return self.color


p = Peacock("Pavo")
p.setFunction("multicolor")
print(p.getFunction())



